'use strict';

const Crypto = require('crypto');
const Joi = require('joi');
const Nonce = require('./lib/nonce');
const { existing } = require('./lib/device');

const PlayerType = {
  Human: 0,
  EasyAI: 2,
  MediumAI: 3,
  HardAI: 4
}

// POST /join_game
// the purpose of all this crypto is to let the user join the game and play without an "account"
module.exports = {
  schema: Joi.object().keys({
    device_id: Joi.string().required(),
    game_id: Joi.string().guid().required(),
    seat: Joi.number().integer().required(),
    type: Joi.number().integer().required().allow(Object.values(PlayerType)),
    signature: Joi.string().required()
  }),
  handler: async (body, db) => {
    let { device_id, game_id, seat, type, signature } = body;
    let [ device, game ] = await Promise.all([
      existing(device_id, db),
      db.ref(`/games/${game_id}`).once('value')
    ]);

    // device is not null -- existing() would throw. check game:
    if(game == null) { throw new Error("Invalid game ID"); }
    else game = game.val();

    let verify = Crypto.createVerify('sha1');
    console.log({ game_id, seat, device });
    verify.update(`${game_id}:${seat}:${device.val.nonce}`);

    const nonce = await Nonce.refresh(device.ref);
    if(!verify.verify(device.val.public_key, Buffer.from(signature, 'hex'))) {
      throw new Error('Failed to verify signature.');
    }

    game.players = game.players || {};

    if(game.players[seat] != null) {
      throw new Error('A player is already occupying that seat.');
    }

    if(seat < 0 || game.seatCount <= seat) {
      throw new Error(`Invalid seat number. The specified game has ${game.seatCount} seats.`);
    }

    const seatData = { device_id, type };
    await db.ref(`/games/${game_id}/players/${seat}`).set(seatData);
    game.players[seat] = seatData;
    return { game, nonce };
  }
};
