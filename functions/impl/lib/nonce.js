'use strict';

const Crypto = require('crypto');

exports.compute = async function compute() {
  return new Promise((res, rej) => {
    Crypto.randomBytes(16, function(err, buffer) {
      if(err) { return rej(err); }
      return res(buffer.toString('hex'));
    });
  });
}

exports.refresh = async function refresh(deviceRef) {
  let nonce = await exports.compute();
  await deviceRef.update({ nonce });
  return nonce;
}
