'use strict';

exports.existing = async (id, db) => {
  const deviceRef = db.ref(`/devices/${id}`);
  const deviceSnapshot = await deviceRef.once('value');
  if(deviceSnapshot == null) throw new Error("Device not found");
  return { ref: deviceRef, val: deviceSnapshot.val() };
}
