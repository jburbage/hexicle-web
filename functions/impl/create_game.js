'use strict';

const Joi = require('joi');

module.exports = {
  schema: Joi.object().keys({
    id: Joi.string().guid().required(),
    board_size: Joi.number().min(3).max(8).required(),
    seat_count: Joi.number().min(2).max(8).required(),
    win_control: Joi.number().min(4).required(),
    version: Joi.number().integer().required()
  }),
  handler: async (body, db) => {
    let { id, board_size, win_control, version, seat_count } = body;
    await db.ref(`/games/${id}`)
      .set(Object.assign({ board_size, win_control, version, seat_count }));

    return { id };
  }
};
