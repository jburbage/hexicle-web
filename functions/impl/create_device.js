'use strict';

const Crypto = require('crypto');
const Nonce = require('./lib/nonce');
const Joi = require('joi');

// This function is not exposed directly (though there's no reason it can't be).
// the save_device function invokes this when the schema matches.
module.exports = {
  schema: Joi.object().keys({
    public_key: Joi.string().required(),
    name: Joi.string().default("Anonymous Human"),
    signature: Joi.string().required()
  }),
  handler: async function createDevice(body, db) {
    const { public_key, signature, name } = body;
    // no device. validate against hard-coded string
    const verify = Crypto.createVerify('sha1');
    verify.update('hexicle:ahoy');

    if(!verify.verify(public_key, Buffer.from(signature, 'hex'))) {
      throw new Error("Failed to verify signature!", { signature, public_key });
    }

    const device_ref = db.ref('/devices');

    // createDevice doesn't consume a nonce, but it needs its first one set
    const nonce = await Nonce.compute();
    const new_device = await device_ref.push({ name, public_key, nonce });
    return { id: new_device.key, nonce };
  }
};