'use strict';

const Crypto = require('crypto');
const Joi = require('joi');
const { existing } = require('./lib/device');
const Nonce = require('./lib/nonce');

// POST /move
// the purpose of all this crypto is to let the user join the game and play without an "account"
module.exports = {
  schema: Joi.object().keys({
    game_id: Joi.string().guid().required(),
    seat: Joi.number().required(),
    action_data: Joi.string().required(),
    signature: Joi.string().required()
  }),
  handler: async (body, db) => {
    const { game_id, seat, action_data, signature } = body;
    const action_json = JSON.parse(action_data);
    let actingPlayer = await db.ref(`/games/${game_id}/players/${seat}`).once('value');
    let movingPlayer = actingPlayer;
    if(seat != action_json.action.Player) {
      movingPlayer = await db.ref(`/games/${game_id}/players/${action_json.action.Player}`).once('value')
    }
    movingPlayer = movingPlayer.val();
    actingPlayer = actingPlayer.val();

    if(movingPlayer == null) {
      throw new Error("The seat specified in the action is empty. Perhaps you meant to join_game?");
    }

    if(actingPlayer == null) {
      throw new Error("The seat specified in the function arguments is empty. No key to use.");
    }

    const { val: device, ref: deviceRef } = await existing(actingPlayer.device_id, db);

    if(movingPlayer.type == 0 && seat != action_json.action.Player) {
      throw new Error('Player attempting to play on behalf of another human.');
    }

    const Verify = Crypto.createVerify('sha1');
    Verify.update(`${game_id}:${action_data}:${device.nonce}`);

    const nonce = await Nonce.refresh(deviceRef);
    if(!Verify.verify(device.public_key, Buffer.from(signature, 'hex'))) {
      throw new Error('Failed to verify signature.', { game_id, action_data, device });
    }

    const new_move = await db.ref(`/games/${game_id}/moves`)
      .push({ seat, action_data });

    return { id: new_move.key, nonce };
  }
};
