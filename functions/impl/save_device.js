'use strict';

const createDevice = require('./create_device');
const updateDevice = require('./update_device');

module.exports = {
  schema: [ createDevice.schema, updateDevice.schema ],
  handler: async (body, db) => body.id ?
    updateDevice.handler(body, db) :
    createDevice.handler(body, db)
};
