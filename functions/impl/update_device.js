'use strict';

const Crypto = require('crypto');
const Joi = require('joi');
const { existing } = require('./lib/device');
const Nonce = require('./lib/nonce');

// This function is not exposed directly (though there's no reason it can't be).
// the save_device function invokes this when the schema matches.
module.exports = {
  schema: Joi.object().keys({
    id: Joi.string().required(),
    name: Joi.string().default("Anonymous Human"),
    signature: Joi.string().required()
  }),
  handler: async function updateDevice(body, db) {
    const { id, name, signature } = body;
    const { val: device, ref: device_ref } = await existing(id, db);

    // device exists. validate with nonce.
    const verify = Crypto.createVerify('sha1');
    verify.update(`${id}:${name}:${device.nonce}`)

    // verify with the saved public key, not the submitted one
    const nonce = await Nonce.refresh(device_ref);
    if(!verify.verify(device.public_key, Buffer.from(signature, 'hex'))) {
      throw new Error("Failed to verify signature!");
    }

    await device_ref.update({ name });
    return { id, nonce };
  }
};