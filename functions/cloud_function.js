const functions = require('firebase-functions');
const Joi = require('joi');

const validate = async (body, schema) => {
  const result = Joi.validate(body, { data: schema }, { abortEarly: false });
  if(result.error) { throw result.error; }
  return result.value.data;
}

module.exports = (func, db) => functions.https.onRequest(async (req, res) => {
  let body;
  try {
    body = await validate(req.body, func.schema);
  } catch (error) {
    console.warn("Validation failed.", error);
    res.status(400).send({ error });
    return;
  }

  let data = await func.handler(body, db);
  res.status(200).send({ data });
});
