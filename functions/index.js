'use strict';

// some boilerplate from the quickstart
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const cloud_function = require('./cloud_function');

admin.initializeApp(functions.config().firebase);
const db = admin.database();

for(let func_name of [ 'save_device', 'create_game', 'join_game', 'move' ]) {
  exports[func_name] = cloud_function(require(`./impl/${func_name}`), db);
}
